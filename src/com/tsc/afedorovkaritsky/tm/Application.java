package com.tsc.afedorovkaritsky.tm;

import com.tsc.afedorovkaritsky.tm.constant.TerminalConst;

public class Application {

    public static void main(String[] args) {
        System.out.println("** WELCOME **");
        parseArgs(args);
    }

    public static void parseArgs(String[] args) {
        if(args == null || args.length == 0) return;
        final String arg = args[0];
        if(TerminalConst.ABOUT.equals(arg)) showAbout();
        if(TerminalConst.VERSION.equals(arg)) showVersion();
        if(TerminalConst.HELP.equals(arg)) showHelp();
    }

    public static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Developer: Aleksandr Fedorov-Karitsky");
        System.out.println("Email: afedorovkaritsky@tsconsulting.com");
    }

    public static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.0.0.0");
    }

    public static void showHelp() {
        System.out.println("[HELP]");
        System.out.println(TerminalConst.ABOUT + " - о разработчике");
        System.out.println(TerminalConst.VERSION + " - версия");
        System.out.println(TerminalConst.HELP + " - помощь");
    }
}
